+++
title = "NUCLEO2USB"
projectdeveloper = "Dmitry Filimonchuk"
projecturl = "https://github.com/dmitrystu/Nucleo2USB"
"made-with-kicad/categories" = [
    "Development Board"
]
+++

link:https://github.com/dmitrystu/Nucleo2USB[NUCLEO2USB] is the USBFS shield for the NUCLEO-64(C) compatible development boards. It can be configured as 1xDEVFS with speed override (F103/F303), 1xOTGFS, 2xOTGFS.
